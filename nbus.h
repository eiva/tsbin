void nbuslock(void);
void nbusunlock(void);
void nbuspreempt(void); 

void nbus_poke16(unsigned char, unsigned short);
unsigned short nbus_peek16(unsigned char);

//void nbus_pokestream16(unsigned char, unsigned char, unsigned short, unsigned char *, int);
//void nbus_peekstream16(unsigned char, unsigned char, unsigned short, unsigned char *, int);
