#
# Tsbin makefile
# 
.PHONY: tshwctl
CROSS_COMPILE = gcc

#
# Include the make variables (CC, etc...)
#
AS			= $(CROSS_COMPILE)as
LD			= $(CROSS_COMPILE)ld
CC			= $(CROSS_COMPILE)$(MCGCC)
AR			= $(CROSS_COMPILE)ar
NM			= $(CROSS_COMPILE)nm
STRIP		= $(CROSS_COMPILE)strip
OBJCOPY		= $(CROSS_COMPILE)objcopy
OBJDUMP		= $(CROSS_COMPILE)objdump

CFLAGS  += -fno-tree-cselim -Wall -O 
LDFLAGS += -lutil

#
# sources and names
#
xuart=xuartctl.c
xuart_exec=xuartctl

tshwctl=tshwctl.c
tshwctl_sources=ispvm.c
tshwctl_exec=tshwctl
tshwctl_objects=${tshwctl_sources:.c=.o}

spiflashctl=spiflashctl.c
spiflashctl_exec=spiflashctl

#
# targets
#
all: tshwctl xuartctl spiflashctl


tshwctl: ${tshwctl_objects}
	${CC} ${CFLAGS} -o ${tshwctl_exec} ${tshwctl} ${tshwctl_objects} ${LDFLAGS}

ispvm.o: ispvm.c
	${CC} -c ispvm.c

xuartctl: ${xuartctl_objects}
	${CC} ${CFLAGS} -o ${xuart_exec} ${xuart} ${LDFLAGS}
	  
spiflashctl:
	${CC} ${CFLAGS} -o ${spiflashctl_exec} ${spiflashctl} ${LDFLAGS}


clean:
	 @echo "Removing files..."
	  rm -f ${tshwctl_objects} ${xuart_exec} ${tshwctl_exec} ${spiflashctl_exec}
	   
help:
	@echo "Avaiable targets"
	@echo " - help (watching now)"
	@echo " - all (to build all binaries)"
	@echo " - tshwctl (to build tshwctl)"
	@echo " - xuartctl (to build xuartctl)"
	@echo " - spiflashctl (to build spiflashctl)"
	@echo " - clean (to cleanup everything)"
