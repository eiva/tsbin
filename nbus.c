/* Copyright 2012, Unpublished Work of Technologic Systems                 
 * All Rights Reserved.                                                    
 *                                                                         
 * THIS WORK IS AN UNPUBLISHED WORK AND CONTAINS CONFIDENTIAL,             
 * PROPRIETARY AND TRADE SECRET INFORMATION OF TECHNOLOGIC SYSTEMS.        
 * ACCESS TO THIS WORK IS RESTRICTED TO (I) TECHNOLOGIC SYSTEMS            
 * EMPLOYEES WHO HAVE A NEED TO KNOW TO PERFORM TASKS WITHIN THE SCOPE     
 * OF THEIR ASSIGNMENTS AND (II) ENTITIES OTHER THAN TECHNOLOGIC           
 * SYSTEMS WHO HAVE ENTERED INTO APPROPRIATE LICENSE AGREEMENTS.  NO       
 * PART OF THIS WORK MAY BE USED, PRACTICED, PERFORMED, COPIED,            
 * DISTRIBUTED, REVISED, MODIFIED, TRANSLATED, ABRIDGED, CONDENSED,        
 * EXPANDED, COLLECTED, COMPILED, LINKED, RECAST, TRANSFORMED, ADAPTED     
 * IN ANY FORM OR BY ANY MEANS, MANUAL, MECHANICAL, CHEMICAL,              
 * ELECTRICAL, ELECTRONIC, OPTICAL, BIOLOGICAL, OR OTHERWISE WITHOUT       
 * THE PRIOR WRITTEN PERMISSION AND CONSENT OF TECHNOLOGIC SYSTEMS.        
 * ANY USE OR EXPLOITATION OF THIS WORK WITHOUT THE PRIOR WRITTEN          
 * CONSENT OF TECHNOLOGIC SYSTEMS COULD SUBJECT THE PERPETRATOR TO         
 * CRIMINAL AND CIVIL LIABILITY.                                           
 */                                                                        
/* To compile nbus in to your application,
 * use the appropriate cross compiler and run the command:
 *                                                                       
 *  gcc -fno-tree-cselim -Wall -O -mcpu=arm9 -o <app> <app.c>  nbus.c
 *                                                                       
 * On uclibc based initrd's, the following additional gcc options are    
 * necessary: -Wl,--rpath,/slib -Wl,-dynamic-linker,/slib/ld-uClibc.so.0 
 */
                                                                           
/* Version history:                                                        
 *                                                                         
 *                                                                         
 */     

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/futex.h>
#include <sched.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>                                                                   
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <semaphore.h>


#include "nbus.h"

#ifndef TEMP_FAILURE_RETRY
# define TEMP_FAILURE_RETRY(expression) \
  (__extension__                                                              \
    ({ long int __result;                                                     \
       do __result = (long int) (expression);                                 \
       while (__result == -1L && errno == EINTR);                             \
       __result; }))
#endif

static int nbuslocked = 0;
unsigned int * volatile mxsgpioregs;
unsigned int * volatile mxsclkregs;

/*XXX: Notes
 * Test DIO twiddling speed
 * test swp on cpu reg and ram 
 * Odd byte accesses are UNDEFINED
 */


void nbus_poke16(unsigned char adr, unsigned short dat)
{
	int did_lock = 0, x;
	
	if(!nbuslocked) {
		nbuslock();
		did_lock++;
	}


	mxsgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
	mxsgpioregs[0x704/4] = ((1 << 26) | adr ); //set adr and ALE
	mxsgpioregs[0x704/4] = (1 << 25); //raise wrn

	for(x = 1; x >= 0; x--) {
        	mxsgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
		mxsgpioregs[0x704/4] = (unsigned char)(dat >> (x*8)); //set dat
		mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
	}
	mxsgpioregs[0x704/4] = (1 << 16) | (1 << 27); //raise CS, cle

	while(mxsgpioregs[0x900/4] & (1 << 21)) {
		mxsgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
		mxsgpioregs[0x704/4] = (1 << 16) | (1 << 27); //raise CS, cle
	}
		

	if(did_lock) nbusunlock();
}

unsigned short nbus_peek16(unsigned char adr)
{
	int did_lock = 0, x;
	unsigned short ret;

	if(!nbuslocked) {
		nbuslock();
		did_lock++;
	}
	mxsgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (1 << 27) | (0xff); //Drop CS, wrn, cle, rdn, and clear dat
	mxsgpioregs[0x704/4] = (1 << 26) | adr; //raise ALE and set adr
	mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
	mxsgpioregs[0xb00/4] = 0xffdfff00; //Set all to output, dat to input

	do {
		ret = 0;
		for(x = 1; x >= 0; x--) {
			mxsgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (1 << 27) | (0xff); //Drop CS, wrn, cle, rdn, and clear dat
			mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
			ret |= ((mxsgpioregs[0x900/4] & 0xff) << (x*8)); //read dat
		}
		mxsgpioregs[0x704/4] = (1 << 16) | (1 << 24) | (1 << 27); //raise CS, rdn, cle
	} while(mxsgpioregs[0x900/4] & (1 << 21)); 

	mxsgpioregs[0xb00/4] = 0xffdfffff; //Set all to output

	if(did_lock) nbusunlock();

	return ret;
}

/* XXX: Stream functions are currently not implemented in the FGPA */
#if 0
void nbus_peekstream16(unsigned char adr_reg, unsigned char dat_reg, unsigned short adr, unsigned short *dat, int count)
{
	int did_lock = 0, i, x;

	if(!nbuslocked) {
		nbuslock();
		did_lock++;
	}

	nbus_poke16(adr_reg, adr);

	mxsgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (0xff); //Drop CS, wrn, rdn, and clear dat
	mxsgpioregs[0x704/4] = (1 << 26) | dat_reg; //raise ALE and set adr
	mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
	mxsgpioregs[0xb00/4] = 0xffdfff00; //Set all to output, dat to input

	for(i = count; i >= 1; i--) {
		*dat = 0;
		/* Try unlikely() */
		if(i == 2) mxsgpioregs[0x708/4] = (1 << 27); //drop CLE to end burst on next cycle
		for(x = 1; x >= 0; x--) {
			mxsgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (0xff); //Drop CS, wrn, rdn, and clear dat
			mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
			*dat |= ((mxsgpioregs[0x900/4] & 0xff) << (x*8)); //set dat
		}
		dat++;
	}

	mxsgpioregs[0x704/4] = (1 << 16) | (1 << 24) | (1 << 27); //raise CS, rdn, cle

	mxsgpioregs[0xb00/4] = 0xffdfffff; //Set all to output
	if(did_lock) nbusunlock();
}

void nbus_pokestream16(unsigned char adr_reg, unsigned char dat_reg, unsigned short adr, unsigned short *dat, int count)
{
	int did_lock = 0, x, i;

	if(!nbuslocked) {
		nbuslock();
		did_lock++;
	}

	nbus_poke16(adr_reg, adr);

	mxsgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (0xff); //Drop CS, wrn, and clear dat
	mxsgpioregs[0x704/4] = ((1 << 26) | dat_reg ); //set adr and ALE
	mxsgpioregs[0x704/4] = (1 << 25); //raise wrn

	for(i = count; i >= 1; i--) {
		if(i == 2) mxsgpioregs[0x708/4] = (1 << 27); //drop CLE to end burst on next cycle
		for(x = 1; x >= 0; x--) {
			/*XXX: need to check for bus error here*/
			mxsgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (0xff); //Drop CS, wrn, and clear dat
			mxsgpioregs[0x704/4] = (unsigned char)(*dat >> (x*8)); //set dat
			mxsgpioregs[0x704/4] = (1 << 25); //raise wrn
		}
		dat++;
	}
	mxsgpioregs[0x704/4] = (1 << 16) | (1 << 27); //raise CS, cle

	if(did_lock) nbusunlock();
}
#endif	

static int semid = -1;
void nbuslock(void)
{
	int devmem = 0;
	int r;
	struct sembuf sop;
	static int inited = 0;

	if(nbuslocked) return;

	if (semid == -1) {
		key_t semkey;
		semkey = 0x75000000;
		semid = semget(semkey, 1, IPC_CREAT|IPC_EXCL|0777);
		if (semid != -1) {
			sop.sem_num = 0;
			sop.sem_op = 1;
			sop.sem_flg = 0;
			r = semop(semid, &sop, 1);
			assert (r != -1);
		} else semid = semget(semkey, 1, 0777);
		assert (semid != -1);
	}
	sop.sem_num = 0;
	sop.sem_op = -1;
	sop.sem_flg = SEM_UNDO;


	/*Wrapper added to retry in case of EINTR*/
	r = TEMP_FAILURE_RETRY(semop(semid, &sop, 1));
	assert(r == 0);

	nbuslocked = 1;

	if(!inited) {
		devmem = open("/dev/mem", O_RDWR|O_SYNC);
		mxsgpioregs = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED,
		  devmem, 0x80018000);
		/* Set all NAND pins to GPIO */
		mxsgpioregs[0x100/4] = 0xffffffff; //Set pinmux to GPIO
		mxsgpioregs[0x110/4] = 0xffffffff; //Set pinmux to GPIO
		mxsgpioregs[0x700/4] = 0xffffffff; //Set all to logic high
		mxsgpioregs[0xb00/4] = 0xffdfffff; //Set all to output
		inited = 1;
	}
}

void nbusunlock(void) 
{
	struct sembuf sop = { 0, 1, SEM_UNDO};
	int r;
	if(nbuslocked) {
		r = semop(semid, &sop, 1);
		assert(r == 0);
		nbuslocked = 0;
	}
}

void nbuspreempt(void)
{
	int r;
	r = semctl(semid, 0, GETNCNT);
	assert(r != -1);
	if(r) {
		nbusunlock();
		sched_yield();
		nbuslock();
	}
}
