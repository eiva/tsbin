/*  Copyright 2004-2012, Unpublished Work of Technologic Systems
*  All Rights Reserved.
*
*  THIS WORK IS AN UNPUBLISHED WORK AND CONTAINS CONFIDENTIAL,
*  PROPRIETARY AND TRADE SECRET INFORMATION OF TECHNOLOGIC SYSTEMS.
*  ACCESS TO THIS WORK IS RESTRICTED TO (I) TECHNOLOGIC SYSTEMS EMPLOYEES
*  WHO HAVE A NEED TO KNOW TO PERFORM TASKS WITHIN THE SCOPE OF THEIR
*  ASSIGNMENTS  AND (II) ENTITIES OTHER THAN TECHNOLOGIC SYSTEMS WHO
*  HAVE ENTERED INTO  APPROPRIATE LICENSE AGREEMENTS.  NO PART OF THIS
*  WORK MAY BE USED, PRACTICED, PERFORMED, COPIED, DISTRIBUTED, REVISED,
*  MODIFIED, TRANSLATED, ABRIDGED, CONDENSED, EXPANDED, COLLECTED,
*  COMPILED,LINKED,RECAST, TRANSFORMED, ADAPTED IN ANY FORM OR BY ANY
*  MEANS,MANUAL, MECHANICAL, CHEMICAL, ELECTRICAL, ELECTRONIC, OPTICAL,
*  BIOLOGICAL, OR OTHERWISE WITHOUT THE PRIOR WRITTEN PERMISSION AND
*  CONSENT OF TECHNOLOGIC SYSTEMS . ANY USE OR EXPLOITATION OF THIS WORK
*  WITHOUT THE PRIOR WRITTEN CONSENT OF TECHNOLOGIC SYSTEMS  COULD
*  SUBJECT THE PERPETRATOR TO CRIMINAL AND CIVIL LIABILITY.
*/
/* To compile tshwctl, use the appropriate cross compiler and run the
* command:
*
*  gcc -fno-tree-cselim -Wall -O -mcpu=arm9 -o tshwctl tshwctl.c ispvm.c
*
* You need ispvm.c and vmopcode.h in the same directory as tshwctl to
* compile.
*
* On uclibc based initrd's, the following additional gcc options are
* necessary: -Wl,--rpath,/slib -Wl,-dynamic-linker,/slib/ld-uClibc.so.0
*
* If you want --loadfpga to work with .jed files, the executable "jed2vme"
* should be in the $PATH during runtime.
*/

const char copyright[] = "Copyright (c) Technologic Systems - " __DATE__ ;

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <libgen.h>
#include <limits.h>
#include <linux/unistd.h>
#include <netdb.h>
#include <net/if.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <time.h>
#include <time.h>
#include <unistd.h>

#ifndef TEMP_FAILURE_RETRY
# define TEMP_FAILURE_RETRY(expression) \
  (__extension__                                                              \
    ({ long int __result;                                                     \
      do __result = (long int) (expression);                                 \
      while (__result == -1L && errno == EINTR);                             \
      __result; }))
#endif

#define DIOMASK 0x33ffffffffffc3dfULL

volatile unsigned int *mxspiregs, *mxgpioregs, *mxtwiregs;
volatile unsigned int *mxmiscregs, *mxtimerregs;
volatile unsigned int *mxgpioregs;
unsigned int gpio_state[3];
int long_tagmem = 0;
static int nbuslocked = 0;

extern signed char ispVM(char *);
void nbuslock(void);
void nbusunlock(void);
void nbuspreempt(void);
void nbus_poke16(unsigned char, unsigned short);
unsigned short nbus_peek16(unsigned char);
//int i2c_write(int chip, int addr, int count, char *buf);

volatile unsigned int *fe_base;

struct vtu {
	int v, vid;
	unsigned char tags[7];
	unsigned char forceVID[7];
};                                

static unsigned short model;
static int cpu_port;
#define FE_BASE_PHYSICAL 0xC0800000                          
#define SMI_RDVALID     (1 << 27)                            
#define SMI_BUSY        (1 << 28)                            
                                                             
#define GLOBAL_REGS_1   0x1f                                 
#define GLOBAL_REGS_2   0x17                                 
                                                             
#define VTU_OPS_REGISTER  0x05  /* offset in GLOBAL_REGS_1 */
#define VTU_OP_FLUSHALL          (1 << 12)                   
#define VTU_OP_LOAD              (3 << 12)                   
#define VTU_OP_GET_NEXT          (4 << 12)                   
#define VTU_OP_GET_CLR_VIOLATION (7 << 12)                   
                                                             
#define VTU_VID_REG     0x06  /* offset in GLOBAL_REGS_1 */  
#define FPGA_SYSCON_PHYSICAL    0x80004000
#define DIO27  1                          
#define DIO42  (1<<15)                    
#define MDIO   DIO27                      
#define MDC    DIO42                      

static unsigned int is_7400(void) __attribute__((pure));
static unsigned int is_7400(void) {
	FILE *cpuinfo;
	char buf[4096];
	static unsigned int checked = 0;
	static unsigned int is4700 = 0;

	if(!checked) {
		cpuinfo = fopen("/proc/cpuinfo", "r");
		if(cpuinfo == NULL) {
			perror("/proc/cpuinfo");
			exit(4);
		}
		bzero(buf, 4096);
		fread(buf, 1, 4095, cpuinfo);
		fclose(cpuinfo);
		if(strstr(buf, "TS-7400")) is4700 = 1;
		checked = 1;
	}
	return is4700;
}

/*XXX: DO NOT USE THESE ROUTINES IN A CUSTOMER APPLICATION UNLESS YOU FULLY
* UNDERSTAND HOW THEY ARE USED AND WHAT THEY DO! See nbus.c from our FTP site
* for the API intended for customer use!
*/
void poke16(unsigned char adr, unsigned short dat)
{
	int x;
	
	mxgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
	mxgpioregs[0x704/4] = ((1 << 26) | adr ); //set adr and ALE
	mxgpioregs[0x704/4] = (1 << 25); //raise wrn

	for(x = 1; x >= 0; x--) {
	mxgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
		mxgpioregs[0x704/4] = (unsigned char)(dat >> (x*8)); //set dat
		mxgpioregs[0x704/4] = (1 << 25); //raise wrn
	}
	mxgpioregs[0x704/4] = (1 << 16) | (1 << 27); //raise CS, cle

	while(mxgpioregs[0x900/4] & (1 << 21)) {
		mxgpioregs[0x708/4] = (1 << 16) | (1 << 26) | (1 << 25) | (1 << 27) | (0xff); //Drop CS, CLE, wrn, and clear dat
		mxgpioregs[0x704/4] = (1 << 16) | (1 << 27); //raise CS, cle
	}
}

unsigned short peek16(unsigned char adr)
{
	int x;
	unsigned short ret;

	mxgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (1 << 27) | (0xff); //Drop CS, wrn, cle, rdn, and clear dat
	mxgpioregs[0x704/4] = (1 << 26) | adr; //raise ALE and set adr
	mxgpioregs[0x704/4] = (1 << 25); //raise wrn
	mxgpioregs[0xb00/4] = 0xffdfff00; //Set all to output, dat to input

	do {
		ret = 0;
		for(x = 1; x >= 0; x--) {
			mxgpioregs[0x708/4] = (1 << 16) | (1 << 25) | (1 << 24) | (1 << 27) | (0xff); //Drop CS, wrn, cle, rdn, and clear dat
			mxgpioregs[0x704/4] = (1 << 25); //raise wrn
			ret |= ((mxgpioregs[0x900/4] & 0xff) << (x*8)); //read dat
		}
		mxgpioregs[0x704/4] = (1 << 16) | (1 << 24) | (1 << 27); //raise CS, rdn, cle
	} while(mxgpioregs[0x900/4] & (1 << 21));

	mxgpioregs[0xb00/4] = 0xffdfffff; //Set all to output

	return ret;
}

static int semid = -1;
void nbuslock(void)
{
	int r;
	struct sembuf sop;

	if (semid == -1) {
		key_t semkey;
		semkey = 0x75000000;
		semid = semget(semkey, 1, IPC_CREAT|IPC_EXCL|0777);
		if (semid != -1) {
			sop.sem_num = 0;
			sop.sem_op = 1;
			sop.sem_flg = 0;
			r = semop(semid, &sop, 1);
			assert (r != -1);
		} else semid = semget(semkey, 1, 0777);
		assert (semid != -1);
	}
	sop.sem_num = 0;
	sop.sem_op = -1;
	sop.sem_flg = SEM_UNDO;


	/*Wrapper added to retry in case of EINTR*/
	r = TEMP_FAILURE_RETRY(semop(semid, &sop, 1));
	assert(r == 0);

	nbuslocked = 1;
}

void nbusunlock(void) 
{
	struct sembuf sop = { 0, 1, SEM_UNDO};
	int r;
	if(nbuslocked) {
		r = semop(semid, &sop, 1);
		assert(r == 0);
		nbuslocked = 0;
	}
}

void nbuspreempt(void)
{
	int r;
	r = semctl(semid, 0, GETNCNT);
	assert(r != -1);
	if(r) {
		nbusunlock();
		sched_yield();
		nbuslock();
	}
}

void sspi_cmd(unsigned int cmd) {
	unsigned int i;
	unsigned int s = peek16(0x6) & 0xfff0;
	
	// pulse CS#
	poke16(0x6, s | 0x2);
	poke16(0x6, s | 0x0);

	for (i = 0; i < 32; i++, cmd <<= 1) {
		if (cmd & 0x80) {
			poke16(0x6, s | 0x4);
			poke16(0x6, s | 0xc);
		} else {
			poke16(0x6, s | 0x0);
			poke16(0x6, s | 0x8);
		}
	}
}


int read_tagmem(unsigned int *tagmem) {
	int i, j, len;
	unsigned int ret;
	unsigned int s = peek16(0x6) & 0xfff0;

	sspi_cmd(0xac); // X_PROGRAM_EN
	sspi_cmd(0x4e); // READ_TAG

	if(long_tagmem) len = 24;
	else len = 20;
	
	for (j = 0; j < len; j++) {
		for (ret = 0x0, i = 0; i < 32; i++) {
			poke16(0x6, s | 0x0);
			poke16(0x6, s | 0x8);
			ret = ret << 1 | (peek16(0x6) & 0x1);
		}
		tagmem[j] = ret;
	}
	
	sspi_cmd(0x78); // PROGRAM_DIS
	
	poke16(0x6, s | 0x2);
	return 1;
}


int write_tagmem(unsigned int *tagmem) {
	int i, j, len;
	unsigned int s = peek16(0x6) & 0xfff0;
	
	sspi_cmd(0xac); // X_PROGRAM_EN
	sspi_cmd(0x8e); // WRITE_TAG

	if(long_tagmem) len = 24;
	else len = 20;
	
	for (j = 0; j < len; j++) {
		unsigned int x = tagmem[j];
		for (i = 0; i < 32; i++, x <<= 1) {
			if (x & 0x80000000UL) {
				poke16(0x6, s | 0x4);
				poke16(0x6, s | 0xc);
			} else {
				poke16(0x6, s | 0x0);
				poke16(0x6, s | 0x8);
			}
			if (!long_tagmem && i == 23 && j == 19) break;
		}
	}
	
	for (i = 0; i < 8; i++) {
		poke16(0x6, s | 0x2);
		poke16(0x6, s | 0xa);
	}
	poke16(0x6, s | 0x2);
	nbusunlock();
	usleep(25000);
	nbuslock();
	return 1;
}


int erase_tagmem() {
	int i;
	unsigned int s = peek16(0x6) & 0xfff0;

	sspi_cmd(0xac); // X_PROGRAM_EN
	sspi_cmd(0xe); // ERASE_TAG

	for (i = 0; i < 8; i++) {
		poke16(0x6, s | 0x2);
		poke16(0x6, s | 0xa);
	}
	poke16(0x6, s | 0x2);
	nbusunlock();
	usleep(1000000);
	nbuslock();
	return 1;
}

static unsigned char rtc_i2c_7bit_adr = 0x6f;
static void i2c_pause(void) { usleep(1); }
#define scl_z do { i2c_pause(); *z = (1 << 24); } while(0)
#define scl_0 do { i2c_pause(); *y = (1 << 24); } while(0)
#define sda_z do { i2c_pause(); *z = (1 << 25); } while(0)
#define sda_0 do { i2c_pause(); *y = (1 << 25); } while(0)
#define sda_in (x[0x930/4] & (1 << 25))

#define rtc_read(reg) rtc_op((reg))
#define rtc_write(reg, val) rtc_op((reg)|((val&0xff)<<8)|(1<<16))
#define rtc_done() rtc_op(-1)
int rtc_op(int op) {
	unsigned int d, i, ack, ret;
	unsigned int reg = op & 0xff;
	static int rtc_state, printed;
	volatile unsigned int *x = mxgpioregs;
	volatile unsigned int *y = &x[0xb34/4];
	volatile unsigned int *z = &x[0xb38/4];
	if (rtc_state == -1) return -1;
	else if (rtc_state > 0 && rtc_state == reg) { /* parked read hit */
		scl_0; /* scl low, tri sda */
		sda_0; /* scl low, sda low */
		scl_z; /* scl high, sda low */
		goto rxbyte;
	} else if (rtc_state > 0 && (rtc_state == (0x100|reg))) {
		/* parked write hit */
		goto txbyte;
	} else if (rtc_state & 0x100) { /* parked write miss */
		sda_0;
		scl_0;
	} else if (rtc_state) { /* parked read miss */
		scl_0; /* scl low, tri sda */
		sda_z; /* scl low, sda high */
		scl_z; /* scl high, sda hi */
		scl_0; /* scl low, tristate sda */
		sda_0; /* scl low, sda low */
	}

	scl_z; /* tristate, scl/sda pulled up */
	sda_z;

	if (op == -1) {
		rtc_state = 0;
		return 0;
	}

	sda_0; /* i2c, start (sda low) */
	for (d = rtc_i2c_7bit_adr<<1, i = 0; i < 8; i++, d <<= 1) {
		scl_0; /* scl low */
		if (d & 0x80) sda_z; else sda_0;
		scl_z; /* scl high */
	}
	scl_0;
	sda_z; /* scl low, tristate sda */
	scl_z; /* scl high, tristate sda */
	ack = sda_in; /* sample ack */
	if (rtc_state == 0) {
		if (!printed) printf("rtc_present=%d\n", ack ? 0 : 1);
		printed=1;
		if (ack) {
			rtc_state = -1;
			return -1;
		}
	}
	assert(ack == 0);
	sda_0;
	for (d = reg, i = 0; i < 8; i++, d <<= 1) {
		scl_0; /* scl low, sda low */
		if (d & 0x80) sda_z; else sda_0;
		scl_z; /* scl high */
	}
	scl_0; /* scl low, tristate sda */
	sda_z;
	scl_z; /* scl high, tristate sda */
	ack = sda_in; /* sample ack */
	assert(ack == 0);

	if (op>>16) goto txbyte;

	scl_0; /* scl low, tristate sda */
	scl_0; /* scl low, sda high */
	scl_z; /* scl high, sda high */
	sda_0; /* scl high, sda lo (repeated start) */
	for (d = rtc_i2c_7bit_adr<<1|1, i = 0; i < 8; i++, d <<= 1) {
		scl_0; /* scl low */
		if (d & 0x80) sda_z; else sda_0;
		scl_z; /* scl high */
	}
	scl_0; /* scl low, tristate sda */
	sda_z;
	scl_z; /* scl high, tristate sda */
	ack = sda_in; /* sample ack */
	assert(ack == 0);

rxbyte:
	for (ret = i = 0; i < 8; i++) {
		scl_0; /* scl low, tri sda */
		sda_z;
		scl_z; /* scl high, tri sda */
		scl_z; /* scl high, tri sda (for timing) */
		ret <<= 1;
		ret |= sda_in ? 1 : 0;
	}
	rtc_state = (reg + 1) & 0xff;
	return ret;

txbyte:
	for (d = op>>8, i = 0; i < 8; i++, d <<= 1) {
		scl_0; /* scl low, sda low */
		if (d & 0x80) sda_z; else sda_0;
		scl_z; /* scl high */
	}
	scl_0; /* scl low, tristate sda */
	sda_z;
	scl_z; /* scl high, tristate sda */
	ack = sda_in; /* sample ack */
	assert(ack == 0);
	rtc_state = ((reg + 1) & 0xff) | 0x100;
	return 0;
}

#define SBC_MDIO_RD  ((mxgpioregs[0x940/4] >> 1) & 0x1)
#define SBC_MDC_RD  ((mxgpioregs[0x940/4] >> 0) & 0x1)
#define SBC_MDIO_OUT  mxgpioregs[0xb44/4] = 0x2
#define SBC_MDIO_IN  mxgpioregs[0xb48/4] = 0x2
#define SBC_MDC_OUT  mxgpioregs[0xb44/4] = 0x1
#define SBC_MDC_IN  mxgpioregs[0xb48/4] = 0x1
#define SBC_MDIO_HI  mxgpioregs[0x744/4] = 0x2
#define SBC_MDIO_LO  mxgpioregs[0x748/4] = 0x2
#define SBC_MDC_HI  mxgpioregs[0x744/4] = 0x1
#define SBC_MDC_LO  mxgpioregs[0x748/4] = 0x1

#define BB_MDIO_RD  ((peek16(0xe) >> 14) & 0x1)
#define BB_MDC_RD  ((peek16(0xe) >> 13) & 0x1)
#define BB_MDIO_OUT  poke16(0x28, (0x80 | 30))
#define BB_MDIO_IN  poke16(0x28, 30)
#define BB_MDC_OUT  poke16(0x28, (0x80 | 29))
#define BB_MDC_IN  poke16(0x28, 29)
#define BB_MDIO_HI  poke16(0x26, (0x80 | 30))
#define BB_MDIO_LO  poke16(0x26, 30)
#define BB_MDC_HI  poke16(0x26, (0x80 | 29))
#define BB_MDC_LO poke16(0x26, 29)

int opt_ethbb = 0;
static int inline MDIO_RD(void) {
	if(!opt_ethbb) return SBC_MDIO_RD;
	else return BB_MDIO_RD;
}
static int inline MDC_RD(void) {
	if(!opt_ethbb) return SBC_MDC_RD;
	else return BB_MDC_RD;
}
static void inline MDIO_OUT(void) {
	if(!opt_ethbb) SBC_MDIO_OUT;
	else BB_MDIO_OUT;
}
static void inline MDIO_IN(void) {
	if(!opt_ethbb) SBC_MDIO_IN;
	else BB_MDIO_IN;
}
static void inline MDC_OUT(void) {
	if(!opt_ethbb) SBC_MDC_OUT;
	else BB_MDC_OUT;
}
static void inline MDC_IN(void) {
	if(!opt_ethbb) SBC_MDC_IN;
	else SBC_MDC_IN;
}
static void inline MDIO_HI(void) {
	if(!opt_ethbb) SBC_MDIO_HI;
	else BB_MDIO_HI;
}
static void inline MDIO_LO(void) {
	if(!opt_ethbb) SBC_MDIO_LO;
	else BB_MDIO_LO;
}
static void inline MDC_HI(void) {
	if(!opt_ethbb) SBC_MDC_HI;
	else BB_MDC_HI;
}
static void inline MDC_LO(void) {
	if(!opt_ethbb) SBC_MDC_LO;
	else BB_MDC_LO;
}

static int phy_write(unsigned long phy, unsigned long reg, unsigned short data) {
	int x;
	unsigned int b;

	if(opt_ethbb) nbuslock();

	MDIO_OUT();
	MDC_OUT();

	MDC_LO();
	MDIO_HI();

	/* preamble, toggle clock high then low */
	for(x=0; x < 32; x++) {
		MDC_HI();
		MDC_LO();
	}
	/* preamble ends with MDIO high and MDC low */

	/* start bit followed by write bit */
	for(x=0; x < 2; x++) {
		MDIO_LO();
		MDC_HI();
		MDC_LO();
		MDIO_HI();
		MDC_HI();
		MDC_LO();
	}
	/* ends with MDIO high and MDC low */

	/* send the PHY address, starting with MSB */
	b = 0x10;
	for(x=0; x < 5; x++) {
		if (phy & b)
		  {MDIO_HI();}
		else
		  {MDIO_LO();}

		MDC_HI();
		MDC_LO();

		b >>= 1;
	}
	/* ends with MDC low, MDIO indeterminate */

	/* send the register address, starting with MSB */
	b = 0x10;
	for(x=0; x < 5; x++) {
		if (reg & b)
		  {MDIO_HI();}
		else
		  {MDIO_LO();}

		MDC_HI();
		MDC_LO();

		b >>= 1;
	}

	/* ends with MDC low, MDIO indeterminate */

	/* clock a one, then clock a zero */
	MDIO_HI();
	MDC_HI();
	MDC_LO();

	MDIO_LO();
	MDC_HI();
	MDC_LO();

	/* send the data, starting with MSB */
	b = 0x8000;
	for(x=0; x < 16; x++) {
		if (data & b)
		 { MDIO_HI();}
		else
		  {MDIO_LO();}

		MDC_HI();
		MDC_LO();

		b >>= 1;
	}

	MDIO_IN();
	MDC_IN();

	if(opt_ethbb) nbusunlock();
	return 0;
}

static int phy_read(unsigned long phy, unsigned long reg,
  unsigned short *data) {
	int x, d;
	unsigned int a,b;

	d = 0;

	if(opt_ethbb) nbuslock();
	MDC_OUT();
	MDIO_OUT();

	MDC_LO();
	MDIO_HI();

	/* preamble, toggle clock high then low */
	for(x=0; x < 32; x++) {
		MDC_HI();
		MDC_LO();
	}
	/* preamble ends with MDIO high and MDC low */

	/* start bit */
	MDIO_LO(); MDC_HI(); MDC_LO();
	MDIO_HI(); MDC_HI(); MDC_LO();
	MDC_HI();  MDC_LO();
	MDIO_LO(); MDC_HI(); MDC_LO();

	/* send the PHY address, starting with MSB */
	b = 0x10;
	for(x=0; x < 5; x++) {
		if (phy & b)
		  {MDIO_HI();}
		else
		  {MDIO_LO();}

		MDC_HI();
		MDC_LO();

		b >>= 1;
		}
		/* ends with MDC low, MDIO indeterminate */

	/* send the register address, starting with MSB */
	b = 0x10;
	for(x=0; x < 5; x++) {
		if (reg & b)
		  {MDIO_HI();}
		else
		  {MDIO_LO();}

		MDC_HI();
		MDC_LO();

		b >>= 1;
	}

	MDIO_IN();
	/* ends with MDC low, MDIO indeterminate */

	/* another clock */
	MDC_HI();
	MDC_LO();

	/* read the data, starting with MSB */
	b = 0x8000;
	for(x=0; x < 16; x++) {
		MDC_HI();
		a = MDIO_RD();

		if (a & 1)
		d |= b;

		MDC_LO();

		b >>= 1;
	}

	MDIO_IN();
	MDC_IN();

	*data = d;
	if(opt_ethbb) nbusunlock();
	return 0;
}

static uint16_t vtu_readwait(unsigned long reg) {
	uint16_t x;
	do {
		if(phy_read(GLOBAL_REGS_1, reg, &x) < 0) 
		  fprintf(stderr, "VTU_Read Timeout to 0x%lX\n", reg);
	} while (x & (1 << 15));

	return x;
}

static const char *MemberTagString(int tag) {
	switch(tag) {
		case 0: return "unmodified";
		case 1: return "untagged";
		case 2: return "tagged";
		default: return "unknown";
	}
}

inline void switch_enphy(unsigned long phy) {
	phy_write(0x17, 0x19, 0xb300);
	phy_write(0x17, 0x18, phy);
}

inline void switch_enflood(unsigned long port) {
	uint16_t data;
	phy_read(port, 0x04, &data);
	phy_write(port, 0x04, data | 0xf);
}

inline void switch_enbcastflood(unsigned long port) {
	phy_write(port, 0x04, 0x7f);
}

inline void switch_forcelink(unsigned long port) {
	uint16_t data;
	phy_read(port, 0x01, &data);
	phy_write(port, 0x01, data | 0x30);
}

static int vtu_add_entry(struct vtu *entry) {
	int i, j, p;
	unsigned short r7, r8, x;
	unsigned short r6[7];

	while(1) {
		if (phy_read(GLOBAL_REGS_1, VTU_OPS_REGISTER, &x) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if (! (x & (1 << 15)))  /* wait for VB=0 in Global Reg 0x05 */
		break;
	}

	phy_write(GLOBAL_REGS_1, VTU_VID_REG, (1 << 12) | entry->vid);

	r7 = entry->tags[0] | (entry->tags[1] << 4) | (entry->tags[2] << 8) |
	  (entry->tags[3] << 12);
	r8 = entry->tags[4] | (entry->tags[5] << 4) | (entry->tags[6] << 8);

	phy_write(GLOBAL_REGS_1, 0x07, r7);
	phy_write(GLOBAL_REGS_1, 0x08, r8);
	phy_write(GLOBAL_REGS_1, VTU_OPS_REGISTER, 0xb000);   /* start the load */

	/* Next, we take care of the VLAN map, which is a per-port
	bitfield at offset 6 */

	while(1) {
		if (phy_read(GLOBAL_REGS_1, VTU_OPS_REGISTER, &x) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if (! (x & (1 << 15)))  /* wait for VB=0 in Global Reg 0x05 */
		break;
	}

	phy_write(GLOBAL_REGS_1, VTU_VID_REG, 0xFFF);

	i=j=0;
	memset(&r6, 0, sizeof(r6));

	while(1) {
		x = 0;
		phy_write(GLOBAL_REGS_1, VTU_OPS_REGISTER, (1 << 15) | VTU_OP_GET_NEXT);

		while(1) {
			if (phy_read(GLOBAL_REGS_1, VTU_OPS_REGISTER, &x) < 0) {
				fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
				return -1;
			}

			if (! (x & (1 << 15)))  /* wait for VB=0 in Global Reg 0x05 */
			break;
		}

		if (phy_read(GLOBAL_REGS_1, VTU_VID_REG, &x) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if ((x & 0xFFF) == 0xFFF)
		break;

		j++;

		if (phy_read(GLOBAL_REGS_1, 0x07, &r7) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if (phy_read(GLOBAL_REGS_1, 0x08, &r8) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		for(p=0; p < 7; p++) {
			switch(p) {
			  case 0: if ((r7 & 0x3) != 0x3)  goto L1; break;
			  case 1: if ((r7 & 0x30) != 0x30)  goto L1; break;
			  case 2: if ((r7 & 0x300) != 0x300)  goto L1; break;
			  case 3: if ((r7 & 0x3000) != 0x3000)  goto L1; break;
			  case 4: if ((r8 & 0x3) != 0x3)  goto L1; break;
			  case 5: if ((r8 & 0x30) != 0x30)  goto L1; break;
			  case 6: if ((r8 & 0x300) != 0x300)  goto L1; break;
			}

			continue;

L1:
			for(i=0; i < 7; i++) {
				if (i != p) {  /* don't set "our" bit in "our" mask register */
					switch(i) {
					  case 0: if ((r7 & 0x3) != 0x3) { r6[p] |= (1 << i);  } break;
					  case 1: if ((r7 & 0x30) != 0x30) { r6[p] |= (1 << i); } break;
					  case 2: if ((r7 & 0x300) != 0x300) { r6[p] |= (1 << i); } break;
					  case 3: if ((r7 & 0x3000) != 0x3000) { r6[p] |= (1 << i);  }  break;
					  case 4: if ((r8 & 0x3) != 0x3) { r6[p] |= (1 << i);  } break;
					  case 5: if ((r8 & 0x30) != 0x30) { r6[p] |= (1 << i); } break;
					  case 6: if ((r8 & 0x300) != 0x300) { r6[p] |= (1 << i); } break;
					}
				}
				else if (p != cpu_port) {
					if (entry->tags[p] != 3) {
						if (entry->forceVID[p])
						phy_write(0x18 + p, 0x7, 0x1000 | entry->vid);
						else
						phy_write(0x18 + p, 0x7, entry->vid);
					}
				}
			}
		}
	}

	for(p=0; p < 7; p++) {
		phy_write(0x18 + p, 0x6, r6[p]);

		if (entry->tags[p] != 3)
		phy_write(0x18 + p, 0x8, 0x480);
	}

	return 0;
}

#if 0
static int vtu_del_entry(unsigned int vid) {
	unsigned short x;

	while(1) {
		if (phy_read(GLOBAL_REGS_1, VTU_OPS_REGISTER, &x) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if (! (x & (1 << 15)))  /* wait for VB=0 in Global Reg 0x05 */
		break;
	}

	phy_write(GLOBAL_REGS_1, VTU_VID_REG, vid);
	/* Note that the V (valid) bit is not set,
	making this a purge operation */
	phy_write(GLOBAL_REGS_1, VTU_OPS_REGISTER, 0xb000);   /* start the load/purge */

	while(1) {
		if (phy_read(GLOBAL_REGS_1, VTU_OPS_REGISTER, &x) < 0) {
			fprintf(stderr, "Timeout %s %d\n", __FILE__, __LINE__);
			return -1;
		}

		if (! (x & (1 << 15)))  /* wait for VB=0 in Global Reg 0x05 */
		break;
	}

	return 0;
}
#endif

static void usage(char **argv) {
	fprintf(stderr, "Usage: %s [OPTION] ...\n"
	  "Technologic Systems TS-76xx / TS-46xx manipulation.\n"
	  "\n"
	  "General options:\n"
	  "  -a, --address=<adr>     8bit address to use with peek16/poke16\n"
	  "  -r, --peek16            Read 16bit register at adr\n"
	  "  -w, --poke16=<dat>      Write 16bit <dat> to <adr>\n"
	  "  -g, --getmac            Display ethernet MAC address\n"
	  "  -s, --setmac=MAC        Set ethernet MAC address\n"
	  "  -R, --reboot            Reboot the board\n"
	  "  -t, --getrtc            Set system time from RTC\n"
	  "  -S, --setrtc            Set RTC time from system\n"
	  "  -o, --rtcinfo           Print RTC info, batt, temp, etc.\n"
	  "  -v, --nvram             Get/Set RTC NVRAM\n"
	  "  -i, --info              Display board FPGA info\n"
	  "  -e, --greenledon        Turn green LED on\n"
	  "  -b, --greenledoff       Turn green LED off\n"
	  "  -c, --redledon          Turn red LED on\n"
	  "  -d, --redledoff         Turn red LED off\n"
	  "  -D, --setdio=<pin>      Sets DDR and sets specified pin(s)\n"
	  "  -O, --clrdio=<pin>      Sets DDR and clears specified pin(s)\n"
	  "  -G, --getdio            Clears DDR and gets DIO pin(s) input value\n"
	  "  -x, --random            Get 16-bit hardware random number\n"
	  "  -W, --watchdog          Daemonize and set up /dev/watchdog\n"
	  "  -n, --setrng            Seed the kernel random number generator\n"
	  "  -X, --resetswitchon     Enable reset switch\n"
	  "  -Y, --resetswitchoff    Disable reset switch\n"
	  "  -l, --loadfpga=FILE     Load FPGA bitstream from FILE\n"
	  "  -q, --cputemp           Print CPU internal and external temperature\n"
	  "  -U, --removejp=JP       Remove soft jumper numbered JP (1-8)\n"
	  "  -J, --setjp=JP          Set soft jumper numbered JP (1-8)\n"
	  "  -k, --txenon=XUART(s)   Enables the TX Enable for an XUART\n"
	  "  -K, --txenoff=XUART(s)  Disables a specified TX Enable\n"
	  "  -h, --help              This help\n",
	  argv[0]
	);

	if(model == 0x4600) {
		fprintf(stderr,
		  "  -j, --bbclkon           Enables a 12.5MHz clock on DIO 3\n"
		  "  -E, --bbclk2on          Enables a 25MHz clock on DIO 37\n"
		  "  -I, --bbclk3on          Enables a 14.28MHz clock on DIO 3\n"
		  "  -H, --bbclkoff          Disables all baseboard clocks\n"
		  "  -u, --touchon           Turns the touchscreen controller on\n"
		  "  -T, --touchoff          Turns the touchscreen controller off\n"
		  "  -B, --baseboard         Display baseboard ID\n"
		  "  -A, --adc               Display MCP3428 ADC readings in millivolts\n"
		  "  -P, --ethvlan           Set each switch port to its own VLAN\n"
		  "  -y, --ethswitch         Set all switch ports to switch mode\n"
		  "  -e, --ethwlan           Sets port A on VLAN, other ports to switch mode\n"
		  "  -C, --ethinfo           Retrieves info on the onboard switch\n"
		  "  -Q  --ethbb             --eth* flags above are issued to baseboard switch\n"
		  );
	}
}

static inline
unsigned char tobcd(unsigned int n) {
	unsigned char ret;
	ret = (n / 10) << 4;
	ret |= n % 10;
	return ret;
}

static inline unsigned int frombcd(unsigned char x) {
	unsigned int ret;
	ret = (x >> 4) * 10;
	ret += x & 0xf;
	return ret;
}

int main(int argc, char **argv) {
	int devmem, i, c;
	unsigned int tagmem[24];
	unsigned int baseboard = 0;
	unsigned short swmod;
	int opt_peek16 = 0, opt_address = 0, opt_poke16 = 0, opt_pokeval = 0;
	int opt_getmac = 0, opt_setmac = 0, opt_reboot = 0, opt_getrtc = 0;
	int opt_setrtc = 0, opt_info = 0, opt_greenledon = 0;
	int opt_greenledoff = 0, opt_redledon = 0, opt_redledoff = 0;
	int opt_setdio = 0, opt_clrdio = 0, opt_getdio = 0;
	int opt_watchdog = 0, opt_resetswitchon = 0, opt_resetswitchoff = 0;
	int opt_cputemp = 0, opt_random = 0, opt_setrng = 0;
	int opt_removejp = 0, opt_setjp = 0;
	int opt_rtc = 0, opt_rtcinfo = 0, opt_nvram = 0;
	int opt_touchon = 0, opt_touchoff = 0;
	int opt_bbclkon = 0, opt_bbclkoff = 0, opt_bbclk2on=0, opt_bbclk3on=0;
	int opt_txenoff = 0, opt_txenon = 0;// opt_canon = 0, opt_canoff = 0;
	int opt_ethvlan = 0, opt_ethswitch = 0, opt_ethinfo = 0, opt_ethwlan=0;
	int opt_baseboard = 0, opt_adc = 0;
	char *setdio = NULL, *clrdio = NULL, *getdio = NULL, *txen_on = NULL;
	char *txen_off = NULL;
	char *opt_mac = NULL, *opt_loadfpga = NULL;
	static struct option long_options[] = {
	  { "help", 0, 0, 'h' },
	  { "peek16", 0, 0, 'r' },
	  { "poke16", 1, 0, 'w' },
	  { "address", 1, 0, 'a' },
	  { "getmac", 0, 0, 'g' },
	  { "setmac", 1, 0, 's' },
	  { "reboot", 0, 0, 'R' },
	  { "getrtc", 0, 0, 't' },
	  { "setrtc", 0, 0, 'S' },
	  { "rtcinfo", 0, 0, 'o' },
	  { "greenledon", 0, 0, 'e'},
	  { "greenledoff", 0, 0, 'b'},
	  { "redledon", 0, 0, 'c'},
	  { "redledoff", 0, 0, 'd'},
	  { "setdio", 1, 0, 'D'},
	  { "clrdio", 1, 0, 'O'},
	  { "getdio", 1, 0, 'G'},
	  { "info", 0, 0, 'i' },
	  { "baseboard", 0, 0, 'B'},
	  { "random", 0, 0, 'x' },
	  { "watchdog", 0, 0, 'W' },
	  { "setrng", 0, 0, 'n' },
	  { "resetswitchon", 0, 0, 'X' },
	  { "resetswitchoff", 0, 0, 'Y' },
	  { "loadfpga", 1, 0, 'l' },
	  { "cputemp", 0, 0, 'q' },
	  { "removejp", 1, 0, 'U' },
	  { "setjp", 1, 0, 'J' },
	  { "nvram", 0, 0, 'v' },
	  { "touchon", 0, 0, 'u' },
	  { "touchoff", 0, 0, 'T' },
	  { "bbclkon", 0, 0, 'j' },
	  { "bbclk2on", 0, 0, 'E' },
	  { "bbclk3on", 0, 0, 'I' },
	  { "bbclkoff", 0, 0, 'H' },
	  { "txenon", 1, 0, 'k' },
	  { "txenoff", 1, 0, 'K' },
	  { "canon", 1, 0, 'N' },
	  { "canoff", 1, 0, 'f' },
	  { "ethvlan", 0, 0, 'P' },
	  { "ethswitch", 0, 0, 'y' },
	  { "ethinfo", 0, 0, 'C' },
	  { "ethwlan", 0, 0, 'e' },
	  { "adc", 0, 0, 'A' },
	  { "ethbb", 0, 0, 'Q' },
	  { 0, 0, 0, 0 }
	};

	devmem = open("/dev/mem", O_RDWR|O_SYNC);
	assert(devmem != -1);
	mxgpioregs = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED,
	  devmem, 0x80018000);
	/* Set all NAND pins to GPIO */
	mxgpioregs[0x100/4] = 0xffffffff; //Set pinmux to GPIO
	mxgpioregs[0x110/4] = 0xffffffff; //Set pinmux to GPIO
	mxgpioregs[0x700/4] = 0xffffffff; //Set all to logic high
	mxgpioregs[0xb00/4] = 0xffdfffff; //Set all to output

	nbuslock();
	model = peek16(0);
	nbusunlock();
	
	while((c = getopt_long(argc, argv,
	  "hra:w:gs:RtSoiebcdD:O:G:xWXYl:nTfqQ:U:J:vrTk:K:N:f:PyjEIHBAu",
	  long_options, NULL)) != -1) {
		switch (c) {
		case 'v':
			opt_nvram = 1;
			opt_rtc = 1;
			break;
		case 'q':
			opt_cputemp = 1;
			break;
		case 'n':
			opt_setrng = 1;
			break;
		case 'l':
			opt_loadfpga = strdup(optarg);
			break;
		case 'X':
			opt_resetswitchon = 1;
			break;
		case 'Y':
			opt_resetswitchoff = 1;
			break;
		case 'W':
			opt_watchdog = 1;
			break;
		case 'D':
			opt_setdio = 1;	
			setdio = strdup(optarg);
			break;
		case 'O':
			opt_clrdio = 1;
			clrdio = strdup(optarg);
			break;
		case 'G':
			opt_getdio = 1;
			getdio = strdup(optarg);
			break;
		case 'e':
			opt_greenledon = 1;
			break;
		case 'b':
			opt_greenledoff = 1;
			break;
		case 'c':
			opt_redledon = 1;
			break;
		case 'd':
			opt_redledoff = 1;
			break;
		case 'i':
			opt_info = 1;
			break;
		case 'B':
			opt_baseboard = 1;
			break;
		case 'S':
			opt_setrtc = 1;
			opt_rtc = 1;
			break;
		case 'r':
			opt_peek16 = 1;
			break;
		case 'a':
			opt_address = strtoul(optarg, NULL, 0);
			break;
		case 'w':
			opt_poke16 = 1;
			opt_pokeval = strtoul(optarg, NULL, 0);
			break;
		case 'g':
			opt_getmac = 1;
			break;
		case 's':
			opt_setmac = 1;
			opt_mac = strdup(optarg);
			break;
		case 'R':
			opt_reboot = 1;
			break;
		case 't':
			opt_getrtc = 1;
			opt_rtc = 1;
			break;
		case 'o':
			opt_rtcinfo = 1;
			opt_rtc = 1;
			break;
		case 'x':
			opt_random = 1;
			break;
		case 'U':
			opt_removejp = strtoull(optarg, NULL, 0);
			break;
		case 'J':
			opt_setjp = strtoull(optarg, NULL, 0);
			break;
		case 'T':
			opt_touchoff = 1;
			break;
		case 'u':
			opt_touchoff = 1;
			opt_touchon = 1;
			opt_baseboard = 1;
			break;
		case 'j':
			opt_bbclkon = 1;
			opt_bbclkoff = 1;
			break;
		case 'E':
			opt_bbclk2on = 1;
			opt_bbclkoff = 1;
			break;
		case 'I':
			opt_bbclk3on = 1;
			opt_bbclkoff = 1;
			break;
		case 'H':
			opt_bbclkoff = 1;
			break;
		case 'k':
			opt_txenon = 1;
			txen_on = strdup(optarg);
			break;
		case 'K':
			opt_txenoff = 1;
			txen_off = strdup(optarg);
			break;
		case 'N':
			//opt_canon = 1;
			break;
		case 'f':
			//opt_canoff = 1;
			break;
		case 'P':
			opt_ethvlan = 1;
			break;
		case 'y':
			opt_ethswitch = 1;
			break;
		case 'C':
			opt_ethinfo = 1;
			break;
		case 'A':
			opt_baseboard = 1;
			opt_adc = 1;
			break;
		case 'Q':
			opt_ethbb = 1;
			break;
		case 'h':
		default:
			usage(argv);
			return(1);
		}
	}
	
	
	if (opt_loadfpga) {
		unsigned int prev_gpio_en, prev_gpio_ddr, prev_gpio_mux;
		signed char x;
		const char * ispvmerr[] = { "pass", "verification fail",
		  "can't find the file", "wrong file type", "file error",
		  "option error", "crc verification error" };

		mxtimerregs = (unsigned int *) mmap(0, getpagesize(),
		  PROT_READ | PROT_WRITE, MAP_SHARED, devmem, 0x80068000);
		prev_gpio_mux = mxgpioregs[0x160/4];
		prev_gpio_en = mxgpioregs[0x730/4];
		prev_gpio_ddr = mxgpioregs[0xb30/4];
		/* switch JTAG pins into GPIO mode */
		mxgpioregs[0x160/4] |= 0xF;
		/* Set dout, clk, tms as outputs */
		mxgpioregs[0xb30/4] |= 0xD;
		mxgpioregs[0xb30/4] &= ~(0x2); /* Din as inputs */

		x = ispVM(opt_loadfpga);
		nbusunlock();
		if (x == 0) {
			printf("loadfpga_ok=1\n");
		} else {
			assert(x < 0);
			printf("loadfpga_ok=0\n");
			printf("loadfpga_error=\"%s\"\n", ispvmerr[-x]);
		}

		mxgpioregs[0xb30/4] = prev_gpio_ddr;
		mxgpioregs[0x730/4] = prev_gpio_en;
		mxgpioregs[0x160/4] = prev_gpio_mux;
	}
	nbuslock();

	if (opt_cputemp) {
		/*XXX: The 7690 is about -5% of the correct temp*/
		signed int temp[2] = {0,0}, x;
		volatile unsigned int *mxlradcregs;

		nbusunlock();
		mxlradcregs = (unsigned int *) mmap(0, getpagesize(),
		  PROT_READ | PROT_WRITE, MAP_SHARED, devmem, 0x80050000);

		mxlradcregs[0x148/4] = 0xf;
		mxlradcregs[0x144/4] = 0x0; //Set LRDAC0 to channel 0
		mxlradcregs[0x50/4] = 0x0; //Clear ch0 reg
		for(x = 0; x < 20; x++) {
			/*
			 * Clear interrupt
			 * Set to 20uA or 300uA mode and enable current source
			 * Start conversion
			 * Poll for sample completion
			 * Pull out samples
			 */
			mxlradcregs[0x18/4] = 0x1;
			if(x < 10) mxlradcregs[0x20/4] = 0x8101;
			else mxlradcregs[0x20/4] = 0x810F;
			mxlradcregs[0x04/4] = 0x1;
			while(!(mxlradcregs[0x10/4] & 0x1));
			if(x < 10) temp[0] += (mxlradcregs[0x50/4] & 0xffff);
			else temp[1] += (mxlradcregs[0x50/4] & 0xffff);
		}
		mxlradcregs[0x28/4] = 0x8100;
		temp[0] = (((temp[1] - temp[0])*1104)-2730000) + 30000;
		//+30000 is for compensation of wire impedence
		printf("external_temp=%d.%d\n", temp[0] / 10000,
		  abs(temp[0] % 10000));
		
		mxlradcregs[0x148/4] = 0xFF;
		mxlradcregs[0x144/4] = 0x98; //Set to temp sense mode
		mxlradcregs[0x28/4] = 0x8300; //Enable temp sense block
		mxlradcregs[0x50/4] = 0x0; //Clear ch0 reg
		mxlradcregs[0x60/4] = 0x0; //Clear ch1 reg
		temp[0] = temp[1] = 0;

		for(x = 0; x < 10; x++) {
			/* Clear interrupts
			 * Schedule readings
			 * Poll for sample completion
			 * Pull out samples*/
			mxlradcregs[0x18/4] = 0x3;
			mxlradcregs[0x4/4] = 0x3;
			while(!((mxlradcregs[0x10/4] & 0x3) == 0x3)) ;
			temp[0] += mxlradcregs[0x60/4] & 0xFFFF;
			temp[1] += mxlradcregs[0x50/4] & 0xFFFF;
		}
		temp[0] = (((temp[0] - temp[1]) * (1012/4)) - 2730000);
		printf("internal_temp=%d.%d\n",temp[0] / 10000,
		  abs(temp[0] % 10000));
		nbuslock();
	}

	if(opt_poke16) {
		poke16(opt_address, opt_pokeval);
	}

	if(opt_peek16) {
		i = peek16(opt_address);
		nbusunlock();
		printf("0x%x\n", i);
		nbuslock();
	}

	if (opt_setmac) {
		/* This uses one time programmable memory. */
		unsigned int a, b, c;
		int r;
		volatile unsigned int *mxocotpregs;
		nbusunlock();

		mxocotpregs = (unsigned int *) mmap(0, getpagesize(),
		  PROT_READ | PROT_WRITE, MAP_SHARED, devmem, 0x8002C000);

		mxocotpregs[0x08/4] = 0x200;
		mxocotpregs[0x0/4] = 0x1000;
		while(mxocotpregs[0x0/4] & 0x100) ; //check busy flag
		if(mxocotpregs[0x20/4] & (1<<25)) {
			printf("MAC address previously set, cannot set\n");
		} else {
			r = sscanf(opt_mac, "%*x:%*x:%*x:%x:%x:%x",  &a,&b,&c);
			assert(r == 3); /* XXX: user arg problem */
			assert(a < 0x100);
			assert(b < 0x100);
			assert(c < 0x100);
			mxocotpregs[0x0/4] = 0x3E770000;
			mxocotpregs[0x10/4] = (1<<25|a<<16|b<<8|c);
		}
		mxocotpregs[0x0/4] = 0x0;
		nbuslock();
	}

	if (opt_getmac) {
		unsigned char a, b, c;
		unsigned int mac, i;
		volatile unsigned int *mxocotpregs;
		mxocotpregs = (unsigned int *) mmap(0, getpagesize(),
		  PROT_READ | PROT_WRITE, MAP_SHARED, devmem, 0x8002C000);

		mxocotpregs[0x08/4] = 0x200;
		mxocotpregs[0x0/4] = 0x1000;
		while(mxocotpregs[0x0/4] & 0x100) ; //check busy flag
		mac = mxocotpregs[0x20/4] & 0xFFFFFF;
		if(!mac) {
			mxocotpregs[0x0/4] = 0x0; //Close the reg first
			mxocotpregs[0x08/4] = 0x200;
			mxocotpregs[0x0/4] = 0x1013;
			while(mxocotpregs[0x0/4] & 0x100) ; //check busy flag
			mac = (unsigned short) mxocotpregs[0x150/4];
			mac |= 0x4f0000;
		}
		a = mac >> 16;
		b = mac >> 8;
		c = mac;
		
		if(!is_7400()) {
			read_tagmem(tagmem);
			mac = peek16(0x0);
			nbusunlock();
		}
		printf("mac=00:d0:69:%02x:%02x:%02x\n", a, b, c);
		printf("shortmac=%02x%02x%02x\n", a, b, c);
		
		if(!is_7400()) {
			printf("model=0x%x\n", mac);
			for (i = 0; i < 8; i++) printf("jp%d=%c\n", i + 1,
			  ((tagmem[0] & (1<<i)) ? '0' : '1'));
			nbuslock();
		}
		mxocotpregs[0x0/4] = 0x0;
	}

	if (opt_setjp || opt_removejp) {
		unsigned int origtag[24];
	        read_tagmem(origtag);
		memcpy(tagmem, origtag, 24 * sizeof (int));

	        if (opt_setjp) tagmem[0] &= (char)~(1<<(opt_setjp-1));
	        else tagmem[0] |= (char)(1<<(opt_removejp-1));
		if(memcmp(origtag, tagmem, 24 * sizeof (int))) {
	        	erase_tagmem();
		        write_tagmem(tagmem);
		}
	}

	if(opt_rtc)
	{
		mxgpioregs = (unsigned int *) mmap(0, getpagesize(),
		  PROT_READ | PROT_WRITE, MAP_SHARED, devmem, 0x80018000);

		gpio_state[0] = mxgpioregs[0x170/4] & 0xf0000;
		gpio_state[1] = mxgpioregs[0xb30/4] & 0x3000000;
		gpio_state[2] = mxgpioregs[0x730/4] & 0x3000000;

		mxgpioregs[0x174/4] = 0xf0000; /* scl/sda GPIO mode */
		mxgpioregs[0xb38/4] = 0x3000000; /* tristate sda/scl */
		mxgpioregs[0x738/4] = 0x3000000; /* sda/scl low */
		usleep(1);
	}

	if(opt_rtcinfo) {
		unsigned short rtcdat[11];
		int i;

		nbusunlock();
		printf("rtctemp_millicelsius=%d\n",
		  ((rtc_read(0x28)|(rtc_read(0x29)<<8))*500)-273000);
		rtc_done();
		rtcdat[0] = rtc_read(0x7);
		rtc_done();
		for(i = 0; i < 5; i++)
		  rtcdat[i+1] = rtc_read(0x16+i);
		rtc_done();
		for(i = 0; i < 5; i++)
		  rtcdat[i+6] = rtc_read(0x1b+i);
		rtc_done();

		printf("rtcinfo_oscillator_ok=%d\n",
		  ((rtcdat[0] & 0x41) ? 0 : 1));
		printf("rtcinfo_batt_low=%d\n",
		  ((rtcdat[0] & 4) >> 2));
		printf("rtcinfo_batt_crit=%d\n",
		  ((rtcdat[0] & 2) >> 1));
		printf("rtcinfo_firstpoweroff=%02x%02x%02x%02x%02x\n",
		  rtcdat[5], rtcdat[4], (rtcdat[3] & 0x7f), rtcdat[2],
		  rtcdat[1]);
		printf("rtcinfo_lastpoweron=%02x%02x%02x%02x%02x\n",
		  rtcdat[10], rtcdat[9], (rtcdat[8] & 0x7f), rtcdat[7],
		  rtcdat[6]);
		rtc_write(0x9, 0x80); //Clear timestamps
		rtc_done();
		nbuslock();
	}

			
	if (opt_setrtc) {
		unsigned char rtcdat[7];
		time_t now = time(NULL);
		struct tm *tm;

		nbusunlock();
		
		rtc_write(0x8, 0x50); /* enable RTC write, disable IRQs on battery */
		rtc_write(0x9, 0);
		rtc_write(0xa, 0x24); /* turn off reseal, set trip levels */
		rtc_write(0xd, (0xc0 | (rtc_read(0xd) & 0x1f))); /* Enable TSE once per 10min */

		tm = gmtime(&now);
		rtcdat[0] = tobcd(tm->tm_sec);
		rtcdat[1] = tobcd(tm->tm_min);
		rtcdat[2] = tobcd(tm->tm_hour) | 0x80; //24 hour bit
		rtcdat[3] = tobcd(tm->tm_mday);
		rtcdat[4] = tobcd(tm->tm_mon + 1);
		assert(tm->tm_year >= 100);
		rtcdat[5] = tobcd(tm->tm_year % 100);
		rtcdat[6] = tm->tm_wday + 1;

		for (i = 0; i < 7; i++) rtc_write(i, rtcdat[i]);
		rtc_done();
		nbuslock();
	}

	if (opt_getrtc) {
	        unsigned char rtcdat[8];
	        time_t now;
	        struct tm tm;
	        struct timeval tv;
	
		nbusunlock();
	        for (i = 0; i < 8; i++) rtcdat[i] = rtc_read(i);
	        rtc_done();
	        printf("rtc_oscillator_ok=%d\n", rtcdat[7] & 1 ? 0 : 1);
	        if (!(rtcdat[7] & 1)) {
	                tm.tm_sec = frombcd(rtcdat[0] & 0x7f);
	                tm.tm_min = frombcd(rtcdat[1] & 0x7f);
	                tm.tm_hour = frombcd(rtcdat[2] & 0x3f);
	                tm.tm_mday = frombcd(rtcdat[3] & 0x3f);
	                tm.tm_mon = frombcd(rtcdat[4] & 0x1f) - 1;
	                tm.tm_year = frombcd(rtcdat[5]) + 100;
	                setenv("TZ", "UTC", 1);
	                now = mktime(&tm);
	                tv.tv_sec = now;
	                tv.tv_usec = 0;
	                settimeofday(&tv, NULL);
	        }
		nbuslock();
	}
	if (opt_nvram) {
	unsigned int dat[32], o, x;
	        unsigned char *cdat = (unsigned char *)dat;
		char *e, var[8];
	
		nbusunlock();
	        o = rtc_i2c_7bit_adr;
	        rtc_i2c_7bit_adr = 0x57; /* NVRAM uses a different I2C adr */
	        for (i = 0; i < 128; i++) cdat[i] = rtc_read(i);
	        for (i = 0; i < 32; i++) {
	                sprintf(var, "nvram%d", i);
	                e = getenv(var);
	                if (e) {
	                        x = strtoul(e, NULL, 0);
	                        rtc_write(i<<2, x);
	                        rtc_write(1+(i<<2), x >> 8);
	                        rtc_write(2+(i<<2), x >> 16);
	                        rtc_write(3+(i<<2), x >> 24);
	                }
	        }
	        rtc_done();
	        rtc_i2c_7bit_adr = o;
	        for (i = 0; i < 32; i++) printf("nvram%d=0x%x\n", i, dat[i]);
		nbuslock();
	}

	if(opt_rtc) {
	   mxgpioregs[0x178/4] = 0xf0000;
	   mxgpioregs[0x174/4] = (gpio_state[0]);		
	   mxgpioregs[0xb38/4] =  0x3000000;		
	   mxgpioregs[0xb34/4] = (gpio_state[1]);		
	   mxgpioregs[0x738/4] =  0x3000000;
	   mxgpioregs[0x734/4] = (gpio_state[2]);
	}

	if(opt_resetswitchon) {
		unsigned short x;
		x = peek16(0x2);
		poke16(0x2, (x | (1 << 11)));
	} else if(opt_resetswitchoff) {
		unsigned short x;
		x = peek16(0x2);
		poke16(0x2, (x & ~(1 << 11)));
	}

	if(opt_info) {
		unsigned short x, y;
		x = peek16(0x0);
		y = peek16(0x2);
		nbusunlock();
		printf("model=0x%x\n", x);
		printf("submodel=0x%x\n", (y >> 4) & 0xf);
		printf("revision=0x%x\n", (y & 0xf));
		printf("bootmode=0x%x\n", (y >> 8) & 0x3);
		printf("bootdev=0x%x\n", (y >> 12) & 0x3);
		printf("fpga_pn=LFXP25E\n");
		printf("resetsw_en=0x%x\n", (y >> 11) & 0x1);
		nbuslock();
	}

	if(opt_setdio) {
		int j = 0, sz = strlen(setdio);
		char *st = 0, *en = 0;
		for(st = setdio; j < sz; st = en + 1) {
			int dio = strtoul(st, &en, 0);
			j += (en - st) + 1;
			poke16(0x26, (0x80 | dio));
			poke16(0x28, (0x80 | dio));
		}
	}

	if(opt_clrdio) {
		int j = 0, sz = strlen(clrdio);
		char *st = 0, *en = 0;
		for(st = clrdio; j < sz; st = en + 1) {
			int dio = strtoul(st, &en, 0);
			j += (en - st) + 1;
			poke16(0x26, (~0x80 & dio));
			poke16(0x28, (0x80 | dio));
		}
	}
	if(opt_getdio) {
		int j = 0, sz = strlen(getdio);
		char *st = 0, *en = 0;
		for(st = getdio; j < sz; st = en + 1) {
			int dio = strtoul(st, &en, 0);
			int value = -1;
			j += (en - st) + 1;
			poke16(0x28, (~0x80 & dio));
			if(dio < 15)
			  value = (peek16(0x08) >> (dio));
			else if (dio < 31)
			  value = (peek16(0x0e) >> (dio - 16));
			else if (dio < 47)
			  value = (peek16(0x14) >> (dio - 32));
			else if (dio < 63)
			  value = (peek16(0x1a) >> (dio - 48));
			else if (dio < 66)
			  value = (peek16(0x20) >> (dio - 64 + 6));
			nbusunlock();
			printf("dio%d=%d\n", dio, value & 0x1);
			nbuslock();
		}
	}

	if(opt_random) {
		unsigned short rand = peek16(0x4);
		nbusunlock();
		printf("random=0x%x\n", rand);
		nbuslock();
	}
	if (opt_setrng) {
		FILE *urandom = NULL;
		unsigned int rng;
		rng = peek16(0x4);
		nbusunlock();
		urandom = fopen("/dev/urandom", "w");
		assert(urandom != NULL);
		fwrite(&rng, 2, 1, urandom);
		fclose(urandom);
		nbuslock();
	}

	if (opt_baseboard && model == 0x4600) {
		unsigned short prev1, prev2, prev3, prev4;
		unsigned int i, x, rev;
		int bb_fd = 0;
		char buf[5] = {0};

		bb_fd = open("/dev/tsbaseboardid", O_RDONLY);
		if(bb_fd > 0) {
			read(bb_fd, &buf, 4);
			buf[4] = '\0';
			baseboard = strtoul(buf, NULL, 0);
		} else {
			prev1 = peek16(0x2);
			prev2 = peek16(0x12);
			prev3 = peek16(0x10);
			prev4 = peek16(0xc);
			poke16(0x12, prev2 | 0x10);
			poke16(0xc, prev4 & ~0x20);
			for(i = 0; i < 8; i++) {
				x = prev1 & ~0xc000;
				if(!(i & 1)) x |= 0x4000;
				if(!(i & 2)) x |= 0x8000;
				if(i & 4) poke16(0x26, 20 | 0x80);
				else poke16(0x26, 20);
				poke16(0x2, x);
				usleep(1);
				baseboard = (baseboard >> 1);
				if(peek16(0x8) & 0x20) baseboard |= 0x80;
			}
			poke16(0x2, prev1);
			poke16(0x12, prev2);
			poke16(0x10, prev3);
			poke16(0xc, prev4);
		}

		rev = (baseboard & 0xc0) >> 6;
		baseboard &= ~0xc0;
		nbusunlock();
		printf("baseboard_model=0x%x\n", baseboard);
		switch (baseboard) {
		  /* The TS-8200 can ID as 63 on early production boards
		   * we do not want to ID by that as well as 63 could
		   * (more likely) be a custom baseboard
		   */
		  case 0:
			printf("baseboard=8200\n");
			break;
		  case 2:
			printf("baseboard=8390\n");
			break;
		  case 4:
			printf("baseboard=8500\n");
			break;
		  case 5:
			printf("baseboard=8400\n");
			break;
		  case 6:
			printf("baseboard=8160\n");
			break;
		  case 7:
			printf("baseboard=8100\n");
			break;
		  case 8:
			printf("baseboard=8820\n");
			break;
		  case 9:
			printf("baseboard=8150\n");
			break;
		  case 10:
			printf("baseboard=8900\n");
			break;
		  case 11:
			printf("baseboard=8290\n");
			break;
		  case 13:
			printf("baseboard=8700\n");
			break;
		  case 14:
			printf("baseboard=8280\n");
			break;
		  case 15:
			printf("baseboard=8380\n");
			break;
		  case 16:
			printf("baseboard=an20\n");
			break;
		}
		printf("baseboard_rev=%c\n", rev + 65);
		nbuslock();
	}

	if(opt_touchoff) poke16(0x2e, (peek16(0x2e) & ~0xc000));
	if(opt_touchon) {
		/* The TS-8380 is a little special and has different SPI pins */
		if(baseboard == 15) poke16(0x2e, (peek16(0x2e) | 0x8000));
		else poke16(0x2e, (peek16(0x2e) | 0x4000));
	}

	if (opt_adc) {
		int x;
		// configure ADC:
		switch (baseboard) {
		  case 2:
			poke16(0x30, 0x28);
			break;
		  case 6:
		  case 7:
			poke16(0x30, 0x18);
			break;
		  default: // if unknown baseboard, uses no an_sel
			// but assumes ADC is there
			poke16(0x30, 0x08);
			break;
		}
		poke16(0x32, 0x3f); // enable all 6 channels
		usleep(500000); // allow time for conversions
		for (i = 1; i <= 6; i++) {
			x = (signed short)peek16(0x32 + 2*i);
			if (i > 2) x = (x * 1006)/200;
			x = (x * 2048)/0x8000;
			printf("adc%d=%d\n", i, x);
		}
	}

	if (opt_ethswitch || opt_ethinfo || opt_ethvlan || opt_ethwlan) {

		nbusunlock();
		assert(model == 0x4600);
		mxgpioregs[0x184/4] = 0xf;

		if(phy_read(0x18, 0x03, &swmod) == -1) {
			printf("switch_model=none\n");
			opt_ethvlan = 0;
			opt_ethwlan = 0;
			opt_ethswitch = 0;
		} else {
			swmod &= 0xFFF0;
			if(swmod == 512){
				printf("switch_model=88E6020\n");
			}else if (swmod == 1792) {
				printf("switch_model=88E6070\n");
			} else if(swmod == 1808) {
				printf("switch_model=88E6071\n");
				fprintf(stderr, "Unsupported switch\n");
			} else if(swmod == 8704) {
				printf("switch_model=88E6220\n");
				fprintf(stderr, "Unsupported switch\n");
			} else if(swmod == 9472) {
				printf("switch_model=88E6251\n");
				fprintf(stderr, "Unsupported switch\n");
			} else {
				printf("switch_model=unknown\n");
			}
		}
		nbuslock();
	}

	if (opt_ethswitch || opt_ethvlan || opt_ethwlan) {
		nbusunlock();
		if(swmod == 512) { // Marvell 88E6020 Network Switch
			// this chip has 2 PHYs and 2 RMII ports
			// enable both PHYs
			switch_enphy(0x9600);
			switch_enphy(0x9620);

			// enable port flood on all ports
			if(opt_ethswitch) {
				switch_enflood(0x18);
				switch_enflood(0x19);
				switch_enflood(0x1d);
			} else if (opt_ethvlan || opt_ethwlan) {
				switch_enbcastflood(0x18);
				switch_enbcastflood(0x19);
				switch_enbcastflood(0x1d);
			}

			// Force link up on P5, the CPU port
			switch_forcelink(0x1d);

			// With this chip ethvlan and wlan switch are the same since
			// we only have 2 ports
			if(opt_ethvlan || opt_ethwlan) {
				struct vtu entry;
				// Configure P5
				phy_write(0x1d, 0x05, 0x0);
				phy_write(0x1d, 0x06, 0x5f);
				phy_write(0x1d, 0x07, 0x1);
				phy_write(0x1d, 0x08, 0x480);

				for(i=0; i < 7; i++) {
					entry.tags[i] = 3; // leave the port alone by default
					entry.forceVID[i] = 0;
				}

				// Set up VLAN #1 on P0 and P5
				entry.v = 1;
				entry.vid = 1;
				entry.tags[5] = 2; // TS-4712 CPU port is always egress tagged
				entry.tags[0] = 1; // External Port A egress untagged
				vtu_add_entry(&entry);
				entry.tags[0] = 3; // Do not configure port A again

				// Setup VLAN #2 on P1 and P5
				entry.vid = 2;
				entry.tags[1] = 1; // External PORT B egress untagged
				vtu_add_entry(&entry);
			}
		} else if (swmod == 1792) { // Marvell 88E6070 Network Switch
			// Used on the TS-8700
			// enable all PHYs
			switch_enphy(0x9600);
			switch_enphy(0x9620);
			switch_enphy(0x9640);
			switch_enphy(0x9660);
			switch_enphy(0x9680);

			switch_enflood(0x18);
			switch_enflood(0x19);
			switch_enflood(0x1a);
			switch_enflood(0x1b);
			switch_enflood(0x1c);
			if(opt_ethvlan) {
				struct vtu entry;
				for(i=0; i < 7; i++) {
					entry.tags[i] = 3; // leave the port alone by default
					entry.forceVID[i] = 0;
				}
				entry.v = 1;

				// Set up VLAN #1 on P0 and P1
				entry.vid = 1;
				entry.tags[0] = 2; // TS-4710 CPU port is always egress tagged
				entry.tags[1] = 1; // External Port A egress untagged
				vtu_add_entry(&entry);
				entry.tags[1] = 3; // Do not configure port A again

				// Setup VLAN #2 on P0 and P2
				entry.vid = 2;
				entry.tags[2] = 1; // External PORT B egress untagged
				vtu_add_entry(&entry);
				entry.tags[2] = 3; // Do not configure port B again

				// Setup VLAN #2 on P0 and P3
				entry.vid = 3;
				entry.tags[3] = 1; // External PORT C egress untagged
				vtu_add_entry(&entry);
				entry.tags[3] = 3; // Do not configure port C again

				// Setup VLAN #2 on P0 and P4
				entry.vid = 4;
				entry.tags[4] = 1; // External PORT D egress untagged
				vtu_add_entry(&entry);
			} else if (opt_ethwlan) {
				struct vtu entry;
				for(i=0; i < 7; i++) {
					entry.tags[i] = 3; // leave the port alone by default
					entry.forceVID[i] = 0;
				}
				entry.v = 1;

				// Set up VLAN #1 with P0 and P1
				entry.vid = 1;
				entry.tags[0] = 2; // TS-4710 CPU port is always egress tagged
				entry.tags[1] = 1; // External Port A egress untagged
				vtu_add_entry(&entry);
				entry.tags[1] = 3; // Do not configure port A again

				// Set up VLAN #1 with P0, P2, P3, and P4
				entry.vid = 2;
				entry.tags[0] = 2; // TS-4710 CPU port is always egress tagged
				entry.tags[2] = 1; // External Port B egress untagged
				entry.tags[3] = 1; // External Port C egress untagged
				entry.tags[4] = 1; // External Port D egress untagged
				vtu_add_entry(&entry);
			}
		} else {
			fprintf(stderr, "Switch not configured\n");
		}
		nbuslock();
	}

	if (opt_ethinfo) {
		int ports = 0, i = 0, port, vtu = 0;
		int phy[8] = {0,0,0,0,0,0,0,0};
		uint16_t x, r7;
		
		nbusunlock();

		if(swmod == 512){ // 4712
			ports = 2;
			phy[0] = 0x18;
			phy[1] = 0x19;
		} else if (swmod == 1792) { // 8700
			ports = 4;
			phy[0] = 0x19;
			phy[1] = 0x1a;
			phy[2] = 0x1b;
			phy[3] = 0x1c;
		} else {
			printf("Unknown switch: %d\n", swmod);
			return 1;
		}
		printf("switch_ports=\"");
		for (i = 0; i < ports; i++) {
			printf("%c", 97 + i);
			if (i != ports - 1)
			  printf(" ");
		}
		printf("\"\n");

		for (i = 0; i < ports; i++) {
			uint16_t dat;
			phy_read(phy[i], 0x0, &dat);
			printf("switchport%c_link=%d\n", 97 + i,
			  dat & 0x1000 ? 1 : 0);
			printf("switchport%c_speed=", 97 + i);
			dat = (dat & 0xf00) >> 8;
			if(dat == 0x8)
			  printf("10HD\n");
			else if (dat == 0x9)
			  printf("100HD\n");
			else if (dat == 0xa)
			  printf("10FD\n");
			else if (dat == 0xb)
			  printf("100FD\n");
		}

		x = vtu_readwait(VTU_OPS_REGISTER);
		phy_write(GLOBAL_REGS_1, VTU_VID_REG, 0xFFF);
		while(1) {
			phy_write(GLOBAL_REGS_1, VTU_OPS_REGISTER, (1 << 15) | VTU_OP_GET_NEXT);
			vtu_readwait(VTU_OPS_REGISTER);
			phy_read(GLOBAL_REGS_1, VTU_VID_REG, &x);

			// No more VTU entries
			if ((x & 0xFFF) == 0xFFF) {
				printf("vtu_total=%d\n", vtu);
				break;
			}
			vtu++;
			printf("vtu%d_vid=%d\n", vtu, x & 0xfff);
			for (port = 0; port < 7; port++) {
				phy_read(0x18 + port, 7, &r7);
				if(port == 0)
				  phy_read(GLOBAL_REGS_1, 0x07, &x);
				else if (port == 4)
				  phy_read(GLOBAL_REGS_1, 0x08, &x);


				switch(port) {
				  case 0:
					if ((x & 0x0003) != 0x0003) {
						printf("vtu%d_port0=1\n"
						  "vtu%d_port0_forcevid=%c\n"
						  "vtu%d_port0_egress=%s\n"
						  "vtu%d_port0_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString(x & 3),
						  vtu, !opt_ethbb ? "a":"cpu");
					}
					break;
				  case 1:
					if ((x & 0x0030) != 0x0030) {
						printf("vtu%d_port1=1\n"
						  "vtu%d_port1_forcevid=%c\n"
						  "vtu%d_port1_egress=%s\n"
						  "vtu%d_port1_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString((x>>4)&3),
						  vtu, !opt_ethbb ? "b":"a");
					}
					break;
				  case 2:
					if ((x & 0x0300) != 0x0300) {
						printf("vtu%d_port2=1\n"
						  "vtu%d_port2_forcevid=%c\n"
						  "vtu%d_port2_egress=%s\n"
						  "vtu%d_port2_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString((x>>8)&3),
						  vtu, !opt_ethbb ? "unused":"b");
					}
					break;
				  case 3:
					if ((x & 0x3000) != 0x3000) {
						printf("vtu%d_port3=1\n"
						  "vtu%d_port3_forcevid=%c\n"
						  "vtu%d_port3_egress=%s\n"
						  "vtu%d_port3_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString((x>>12)&3),
						  vtu, !opt_ethbb ? "unused":"c");
					}
					break;
				  case 4:
					if ((x & 0x0003) != 0x0003) {
						printf("vtu%d_port4=1\n"
						  "vtu%d_port4_forcevid=%c\n"
						  "vtu%d_port4_egress=%s\n"
						  "vtu%d_port4_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString(x & 3),
						  vtu, !opt_ethbb ? "b":"d");
					}
					break;
				  case 5:
					if ((x & 0x0030) != 0x0030) {
						printf("vtu%d_port5=1\n"
						  "vtu%d_port5_forcevid=%c\n"
						  "vtu%d_port5_egress=%s\n"
						  "vtu%d_port5_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString((x>>4)&3),
						  vtu, !opt_ethbb ? "cpu":"unused");
					}
					break;
				  case 6:
					if ((x & 0x0300) != 0x0300) {
						printf("vtu%d_port6=1\n"
						  "vtu%d_port6_forcevid=%c\n"
						  "vtu%d_port6_egress=%s\n"
						  "vtu%d_port6_alias=%s\n",
						  vtu,
						  vtu, r7 & 0x1000? 'Y':'N',
						  vtu, MemberTagString(x & 3),
						  vtu, "unused");
					}
					break;
				}
			}
		}
		nbuslock();
	}

	if(opt_greenledon) poke16(0x2, (peek16(0x2) | 0x8000));
	if(opt_greenledoff) poke16(0x2, (peek16(0x2) & ~0x8000));
	if(opt_redledon) poke16(0x2, (peek16(0x2) | 0x4000));
	if(opt_redledoff) poke16(0x2, (peek16(0x2) & ~0x4000));

	if(opt_txenon) {
		int j = 0, sz = strlen(txen_on);
		char *st = 0, *en = 0;
		for(st = txen_on; j < sz; st = en + 1) {
			int xu = strtoul(st, &en, 0);
			j += (en - st) + 1;
			if(xu >= 0 && xu <= 7)
			  poke16(0x2e, (peek16(0x2e) | (1 << xu)));
		}
	}
	if(opt_txenoff) {
		int j = 0, sz = strlen(txen_off);
		char *st = 0, *en = 0;
		for(st = txen_off; j < sz; st = en + 1) {
			int xu = strtoul(st, &en, 0);
			j += (en - st) + 1;
			if(xu >= 0 && xu <= 7)
			  poke16(0x2e, (peek16(0x2e) & ~(1 << xu)));
		}
	}

	if (opt_reboot) {
		struct sched_param sched;
		sync();
		poke16(0x2a, 0);
		sched.sched_priority = 99;
		sched_setscheduler(0, SCHED_FIFO, &sched);
		while(1);
	}

	if(opt_bbclkoff) poke16(0x2e, (peek16(0x2e) & ~(0x300)));
	if(opt_bbclkon) poke16(0x2e, (peek16(0x2e) | (0x100)));
	if(opt_bbclk2on) poke16(0x2e, (peek16(0x2e) | (0x300)));
	if(opt_bbclk3on) poke16(0x2e, (peek16(0x2e) | (0x200)));
		
		
	/* Should be last! */
	if (opt_watchdog) {
		struct sched_param sched;
		int fd, r, grain = 0, mode, t;
		char c, time = 3;
		unsigned short x = 0;
		fd_set rfds;
		struct timeval tv_now, tv_wait, tv_exp;
		nbusunlock();

#define FEED0 338000
#define FEED1 2706000
#define FEED2 10824000
		/* Single character writes to /dev/watchdog keep it fed */
		/* XXX: DO NOT RUN MULTIPLE INSTANCES OF THIS! IT WILL 
		 * RESULT IN UNDEFINED BEHAVIOR!
		 */
		/* New /dev/watchdog interface:
		 * - Still supports the standard interface that expects
		 *   '0' '1' '2' or '3'.
		 * - Most other characters are still treated as 3.
		 * - 'f' (for feed) is a special character used to create
		 *   automatic feeds.
		 * - 'f' is expected to be followed by three digits, eg "f100".
		 * - Those digits are interpreted as a 3 digit number, which
		 *   is then divided by 10.  For example, "f456" feeds the
		 *   watchdog for 45.6 seconds.
		 * - 'a' (for autofeed) is a special character used to 
		 *   create an autofeed situation
		 * - In order to guarantee timeout, 'a' is expected to be
		 *   followed by a single number, 0 through 3, eg "a1"
		 * - This single number is the standard WDT interface
		 *   and is not real world time
		 * - An extended feed is canceled by any new write to
		 *   /dev/watchdog.
		 * - If 'f' is not followed by 3 digits, the behavior is
		 *   undefined, but in practice you typically just fed the
		 *   wdt for a long time.
		 * - If 'a' is not followed by a single digit, the behavior 
		 *   is undefined.
		 */

		/* We need to make sure we create the pipe before daemonizing
		 * so that as soon as the call to tshwctl -W completes we can
		 * write to the pipe.  The write side of the pipe NEEDS to be 
		 * opened with O_WRONLY and !O_NONBLOCK in order to ensure
		 * that any immediate writes will block until we open the 
		 * pipe here 
		 *
		 * bash redirects open the file with O_WRONLY and !O_NONBLOCK
		 */
		mknod("/dev/watchdog", S_IFIFO|0666, 0);
		daemon(0, 0);
		sched.sched_priority = 98;
		sched_setscheduler(0, SCHED_FIFO, &sched);
		mode = 0; // mode 1 == processing a multi-feed write
		while(1) {
			fd = open("/dev/watchdog", O_RDONLY | O_NONBLOCK);
			assert (fd != -1);
			FD_ZERO(&rfds);
			FD_SET(fd, &rfds);
			if (mode)
				select(fd + 1, &rfds, NULL, NULL, &tv_wait);
			else
				select(fd + 1, &rfds, NULL, NULL, NULL);
			r = 0;
			if (FD_ISSET(fd, &rfds)) r = read(fd, &c, 1);
			if (r) { // data coming in /dev/watchdog
				if (c == 'f') { // new style feed
					read(fd, &c, 1);
					t = (c - '0') * 100;
					read(fd, &c, 1);
					t += (c - '0') * 10;
					read(fd, &c, 1);
					t += (c - '0');
					t = t * 100000;
					x = 0;
					if (t > FEED1) x = 1;
					if (t > FEED2) x = 2;
					nbuslock();
					poke16(0x2a, x);
					nbusunlock();
					switch (x) {
						case 0: grain = FEED0; break;
						case 1: grain = FEED1; break;
						case 2: grain = FEED2; break;
					}
					mode = 1;
					gettimeofday(&tv_exp, NULL);
					t += tv_exp.tv_usec;
					tv_exp.tv_usec = t % 1000000;
					tv_exp.tv_sec += t / 1000000;
				/*XXX: This is needed because close() is not
				 * guaranteed to have closed and flushed the
				 * pipe by the time we re-open and read again
				 */ 
				} else if(c == '\n') { 
				} else { // old style feed
					mode = 0;
					if(c == 'a') {
						mode = 2;
						read(fd, &c, 1);
					}
					if (c > '3' || c < '0') c = '3';
					nbuslock();
					poke16(0x2a, c - '0');
					nbusunlock();
					if(c == '3') mode = 0;
					/* See XXX: above */
					time = (c - '0');
				}
			}
			if (mode == 1) {
				// if in mode 1, we feed wdt and calculate
				// how long to sleep
				nbuslock();
				poke16(0x2a, x);
				nbusunlock();
				gettimeofday(&tv_now, NULL);
				t = tv_exp.tv_sec - tv_now.tv_sec;
				t *= 1000000;
				t += tv_exp.tv_usec;
				t -= tv_now.tv_usec;
				if (t <= grain) mode = 0;
				else if (t <= 3*grain/2) t -= grain;
				else t = grain/2;
				tv_wait.tv_sec = t / 1000000;
				tv_wait.tv_usec = t % 1000000;
			}
			if(mode == 2) {
				nbuslock();
				poke16(0x2a, time);
				nbusunlock();
				switch (time) {
					case 0: 
					  tv_wait.tv_sec = (FEED0 / 2) / 1000000;
					  tv_wait.tv_usec = (FEED0 / 2) % 1000000;
					  break;
					case 1:
					  tv_wait.tv_sec = (FEED1 / 2) / 1000000;
					  tv_wait.tv_usec = (FEED1 / 2) % 1000000;
					  break;
					case 2:
					  tv_wait.tv_sec = (FEED2 / 2) / 1000000;
					  tv_wait.tv_usec = (FEED2 / 2) % 1000000;
					  break;
				}
			}
			// if mode was 0 and no character was read, don't do
			// anything, just go back to waiting for a character
			close(fd);
		} // end forever loop
	}

	nbusunlock();
	return 0;
}
